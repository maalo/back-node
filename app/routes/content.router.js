// user.route.js
"use strict";
var multer = require("multer");
var express = require("express");
var router = express.Router();
module.exports = router;
var contentController = require("../controllers/content.controller");
var multerMiddleware = multer({ "dest": "./tmp"});

// retourne la liste des métadonnées de contenu de slides disponibles sur le serveur ([content.id].meta.json)
router.route('/contents')
    .get((request, response) => {
        contentController.readAll((err, contents)=>{
            if(err) throw err;
            response.end(JSON.stringify(contents, null, 2));
        });
    })
    .post(multerMiddleware.single("file"), (request, response) => {
        console.dir(request.body);
        contentController.create(request.body, request.file, (err, contents)=>{
            if(err) throw err;
            response.end("Contenu enregistré !");
        });
    });


router.route('/contents/:contentId')
.get((request, response) => {
    contentController.read(
      request.params.contentId, (err, success)=>{
        if(err) throw err;
        response.end(JSON.stringify(success, null, 2));
      }
    );
});


/*router.param('contentId', function(req, res, next, id) {
  req.contentId = id;
  next();
});*/
     //.post();

/*router.route('/contents/:contentId')
     .get(contentController.read((err, content) => {
       console.log("here contentId");
       if(err) throw err;
        response.end(JSON.stringify(content, null, 2));
     }));*/

   