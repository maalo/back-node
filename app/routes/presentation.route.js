var CONFIG = JSON.parse(process.env.CONFIG);

var express = require("express");
var fs = require('fs');
var path = require('path');
var ContentModel = require('../models/content.model');


var router = express.Router();
module.exports = router;

//Router
var presDir = CONFIG.presentationDirectory;

// On va charger les présentations présentent dans le dossier de présentation
router.route("/loadPres")
    .get((request, response) => {
        fs.readdir(presDir, (err, files) => {
            let pres = files
            .map((fileName) => path.join(presDir, fileName))
            .filter((filename)=> filename.includes(".pres.json")) //récup fichier finissant par pres.json : path.extname
            .map((file) => require(file))
            .reduce((acc, file) => {
                f_obj={};
                f_obj[file.id]=file;
                return Object.assign({},f_obj, acc);
            }, {});
            console.dir(pres);
            response.end(JSON.stringify(pres, null, 2));
        });
    });

// On va sauvegarder le fichier JSON passé en paramètre dans le dossier présentation
router.route("/savePres")
    .post((request, response) => {
        let id = request.body.id;
    
        fs.writeFile(presDir+id+".pres.json",JSON.stringify(request.body), function(err){
            if(err) throw err;
            response.end("file saved");
        });
    });