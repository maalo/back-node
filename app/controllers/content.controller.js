var CONFIG = JSON.parse(process.env.CONFIG);
var tmpDirectory = CONFIG.tmpDirectory;
var fs = require("fs"); 
var path = require("path");
var ContentModel = require('../models/content.model');
var utils = require('../utils/utils');

class ContentController{
    constructor() {
    }

    static create(content, file, callback){
        let contentModel = new ContentModel();
        if(!!content && !(content instanceof ContentModel)){
            contentModel.fillField(content);
            contentModel.id = utils.generateUUID();
            console.dir(contentModel);
        }
        var pathConcat =  tmpDirectory + file.path;
        console.log("path=     ", file.path);
        console.log("hh--" + pathConcat);      

        if(file){
            
            utils.readFileIfExists(pathConcat,(err, data)=> {
                if(err) return callback(err);
                contentModel.setData(data);
                contentModel.fileName = utils.getNewFileName(contentModel.id, file.originalname);
                contentModel.src = utils.getDataFilePath(contentModel.fileName);
                console.log("hiiiihi" + contentModel.fileName);
                ContentModel.create(contentModel, callback);
            });
        }
        
    };

    static read(id, callback){
        ContentModel.read(id, callback);
    };

    static readAll(callback){
        ContentModel.readAll(callback);
    }
}
module.exports = ContentController;