var CONFIG = require('../../config.json');
var fs = require('fs');
var path = require('path');

class ContentModel{
    constructor() {
        this.type = "";
        this.id = 0;
        this.src = "";
        this.fileName = "";
        this.title = "";

        // Data est private
        var data;
        this.getData = () => data; 
        this.setData = (newData) =>  data = newData;
    }
    
    // On récupère les informations du ContentModel
    fillField(dataJson){
        this.type = dataJson.type;
        this.id = dataJson.id;
        this.src = dataJson.src;
        this.fileName = dataJson.fileName;
        this.title = dataJson.title;
        this.setData(null);
    }
    
    // On va créer un ou deux fichier(s) content en fonction des informations de type ContentModel passé en paramètre
    static create(content, callback){
        if(!!content && !content.id){
            return callback(new Error('ID null'));
        }
        if(!!content && !(content instanceof ContentModel)){
            return callback(new Error("Le contenu n'est pas de type ContentModel"));
        }
        // On créé le fichier contenant l'image dans le cas d'un contenu de type image
        if(content.getData() !== null){
            fs.writeFile(CONFIG.contentDirectory+content.fileName,content.getData(), function(err){
                if(err) return callback(err);
                console.log("file img saved");

                fs.writeFile(CONFIG.contentDirectory+content.id+".meta.json",JSON.stringify(content), function(err){
                    if(err)  callback(err);
                    console.log("file metadata saved");
                    return callback(null, content);
                }); 
            });
        }
        else{
            fs.writeFile(CONFIG.contentDirectory+content.id+".meta.json", JSON.stringify(content), function(err){
                if(err) return callback(err);
                console.log("file metadata saved");
                return callback(null, content);
            });
        }
    }

    // Lecture d'un fichier metadata de content
    static read(id, callback){
        fs.readFile(CONFIG.contentDirectory+id+".meta.json", function(err, data){
            if(err) return callback(err);
            var contentModel = new ContentModel();
            contentModel.fillField(JSON.parse(data));
            return callback(null, contentModel);
        });
    }

    static readAll(callback){
        fs.readdir(CONFIG.contentDirectory, (err, files) => {
            if(err) return callback(err);
            let f_obj = {};
            let meta = files
            .map((fileName) => path.join(CONFIG.contentDirectory, fileName))
            .filter((filename)=> filename.includes(".meta.json")) //récup fichier finissant par pres.json : path.extname
            .map((file) => require(file))
            .reduce((acc, file) => {
                f_obj={};
                f_obj[file.id]=file;
                return Object.assign({},f_obj, acc);
            }, {});
            console.dir(meta);
            return callback(null, meta);
        });
    }

    // On met à jour un fichier de content
    // Ecrase le fichier metadata si déjà existant
    static update(content, callback){
        if(!!content && !content.id){
            return callback(new Error('ID null'));
        }
        if(!!content && !(content instanceof ContentModel)){
            return callback(new Error("Le contenu n'est pas de type ContentModel"));
        }
        // on teste la présence du ficier de métadata
        ContentModel.read(content.id, (err, success)=>{
            if(err){
                return callback(err);
            }else{
                ContentModel.create(content, callback);
            }
        });
    }

    // on supprime le/les fichiers concernant l'ID passé en paramètre
    static delete(id, callback){
        if(id !== null){
            fs.readdir(CONFIG.contentDirectory, function (err, files){
                if(err){
                    console.log(err.message);
                    return callback(err);
                }
                files.forEach(function (file) {
                    if(file === (id+path.extname(file)) || file === (id+".meta.json")){
                        fs.unlink(CONFIG.contentDirectory + "/" + file,function(err){
                            if(err){
                                console.log(err.message);
                                return callback(err);
                            }
                            else{
                                console.log("fichier" + file + " supprimé");
                                return callback(null,"ok");
                            }
                        });
                    }
                });
            });
        }
    }
}

module.exports = ContentModel;