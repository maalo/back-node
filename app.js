var CONFIG = require("./config.json");
process.env.CONFIG = JSON.stringify(CONFIG);
console.log(process.env.CONFIG);

var express = require("express");
var http = require("http");
var path = require("path");
var bodyParser = require("body-parser");

var defaultRoute = require("./app/routes/default.route.js");
var presRoute = require("./app/routes/presentation.route.js");
var router = require("./app/routes/content.router.js");


var app = express();
var server = http.createServer(app);


//init server
app.use(defaultRoute);
app.use(bodyParser.json()); //to support JSON enconded body
app.use(bodyParser.urlencoded({ //to support URL encoded body
    extended: true
}));
app.use(presRoute);
app.use("/admin", express.static(path.join(__dirname, "public/admin")));
app.use("/watch", express.static(path.join(__dirname, "public/watch")));
app.use(router);

server.listen(CONFIG.port);
